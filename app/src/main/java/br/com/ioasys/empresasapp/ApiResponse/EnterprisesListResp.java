package br.com.ioasys.empresasapp.ApiResponse;

import java.util.List;

import br.com.ioasys.empresasapp.Model.Enterprise;

/**
 * Created by William on 02/08/17.
 * Classe java para a
 */

public class EnterprisesListResp {
	private List<Enterprise> enterprises;

	public List<Enterprise> getEnterprises() {
		return enterprises;
	}

	public void setEnterprises(List<Enterprise> enterprises) {
		this.enterprises = enterprises;
	}
}
