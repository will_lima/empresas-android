package br.com.ioasys.empresasapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import br.com.ioasys.empresasapp.Adapters.EnterprisesCustomAdapter;
import br.com.ioasys.empresasapp.ApiResponse.EnterprisesListResp;
import br.com.ioasys.empresasapp.Model.CustomHeaders;
import br.com.ioasys.empresasapp.Model.Enterprise;
import br.com.ioasys.empresasapp.Util.SearchViewFormatter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {
	private ListView listViewComapanies;
	private CustomHeaders customHeaders;
	private TextView textViewStartSearch;
	private ProgressDialog mProgressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);
		getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		getSupportActionBar().setCustomView(R.layout.custom_actionbar);
		customHeaders = (CustomHeaders) getIntent().getExtras().get("headers");
		listViewComapanies = (ListView) findViewById(R.id.listViewComapanies);
		textViewStartSearch = (TextView)findViewById(R.id.textViewStartSearch);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.search_action_bar,menu);
		MenuItem item = menu.findItem(R.id.action_search);
		SearchView searchView = (SearchView) item.getActionView();
		searchView.setOnQueryTextListener(this);
		new SearchViewFormatter()
				.setSearchHintColorResource(R.color.pink)
				.setSearchCloseIconResource(R.drawable.ic_close)
				.setSearchIconResource(R.drawable.ic_search_copy, true, true)
				.setSearchHintText(getString(R.string.search_hint))
				.format(searchView);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		showProgress();
		Retrofit call = new Retrofit.Builder()
				.baseUrl(ApiService.BASE_URL)
				.addConverterFactory(GsonConverterFactory.create())
				.build();
		ApiService service = call.create(ApiService.class);
		Call<EnterprisesListResp> searchRequest = service.searchEnterprises("1",
				query,
				customHeaders.getAccessToken(),
				customHeaders.getClient(),
				customHeaders.getUid());
		searchRequest.enqueue(new Callback<EnterprisesListResp>() {
			@Override
			public void onResponse(Call<EnterprisesListResp> call, Response<EnterprisesListResp> response) {
				if (response.isSuccessful()){
					Log.e("teste", "onResponse: "+response.body().toString());
					if(response.body().getEnterprises().size() > 0){
						ArrayAdapter<Enterprise> adapterEnterprises = new EnterprisesCustomAdapter(
								getApplicationContext(),response.body().getEnterprises(),customHeaders);
						listViewComapanies.setAdapter(adapterEnterprises);
						textViewStartSearch.setVisibility(View.INVISIBLE);
					}else{
						error(getString(R.string.try_again), getString(R.string.errorVoid));
					}

				}else{
					error(getString(R.string.try_again), getString(R.string.errorConnect));
				}
				mProgressDialog.hide();
			}

			@Override
			public void onFailure(Call<EnterprisesListResp> call, Throwable t) {
				//TODO: Avisar que houve um erro
				mProgressDialog.hide();
				error(getString(R.string.try_again), getString(R.string.errorConnect));
			}
		});
		return false;
	}

	@Override
	public boolean onQueryTextChange(String s) {
		return false;
	}

	private void showProgress() {
		this.runOnUiThread(new Runnable() {
			public void run() {
				mProgressDialog = new ProgressDialog(SearchActivity.this);
				mProgressDialog.setIndeterminate(true);
				mProgressDialog.setMessage(getString(R.string.loading));
				mProgressDialog.setCancelable(false);
				mProgressDialog.show();
			}
		});
	}

	private void error(String message, String title) {
		new AlertDialog.Builder(this)
				.setIcon(R.drawable.ic_warning_amber_800_24dp)
				.setTitle(title)
				.setMessage(message)
				.setNeutralButton(getString(R.string.ok), null)
				.show();
	}
}
