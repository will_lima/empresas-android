package br.com.ioasys.empresasapp.ApiResponse;

import br.com.ioasys.empresasapp.Model.Investor;

/**
 * Created by William on 27/07/17.
 * Classe java para a
 */

public class LoginResp {

	private boolean sucess;
	private String enterprise;
	private Investor investor;

	public String getEnterprise() {
		return enterprise;
	}

	public void setEnterprise(String enterprise) {
		this.enterprise = enterprise;
	}

	public Investor getInvestor() {
		return investor;
	}

	public void setInvestor(Investor investor) {
		this.investor = investor;
	}

	public boolean isSucess() {
		return sucess;
	}

	public void setSucess(boolean sucess) {
		this.sucess = sucess;
	}

}
