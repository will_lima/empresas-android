package br.com.ioasys.empresasapp;

import br.com.ioasys.empresasapp.ApiResponse.EnterpriseResp;
import br.com.ioasys.empresasapp.ApiResponse.EnterprisesListResp;
import br.com.ioasys.empresasapp.ApiResponse.LoginResp;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by William on 27/07/17.
 * Classe java para a
 */

public interface ApiService {
	String server = "http://54.94.179.135:8090";
	String version = "v1";
	String BASE_URL = server+"/api/"+version+"/";

	@FormUrlEncoded
	@POST("users/auth/sign_in")
	Call<LoginResp> login(@Field("email") String email,
	                      @Field("password") String password);

	@GET("enterprises")
	Call<EnterprisesListResp> searchEnterprises(
			@Query("enterprise_type") String tipo,
			@Query("name") String name,
	        @Header("access-token") String token,
	        @Header("client") String client,
	        @Header("uid") String uid);

	@GET("enterprises/{id}")
	Call<EnterpriseResp> getEnterprise(@Path("id") int id,
	                                   @Header("access-token") String token,
	                                   @Header("client") String client,
	                                   @Header("uid") String uid
	);
}
