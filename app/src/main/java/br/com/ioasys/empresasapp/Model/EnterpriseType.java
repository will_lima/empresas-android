package br.com.ioasys.empresasapp.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by William on 02/08/17.
 * Classe java para a
 */

public class EnterpriseType {
	private Integer id;
	@SerializedName("enterprise_type_name")
	private String enterpriseTypeName;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEnterpriseTypeName() {
		return enterpriseTypeName;
	}

	public void setEnterpriseTypeName(String enterpriseTypeName) {
		this.enterpriseTypeName = enterpriseTypeName;
	}
}
