package br.com.ioasys.empresasapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.ioasys.empresasapp.ApiResponse.EnterpriseResp;
import br.com.ioasys.empresasapp.Model.CustomHeaders;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DescActivity extends AppCompatActivity {
	private ProgressDialog mProgressDialog;
	private TextView textViewDesc;
	private ImageView imageViewPhoto;
	private CustomHeaders customHeaders;
	private Integer idEnterprise;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_desc);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		textViewDesc = (TextView) findViewById(R.id.textViewDesc);
		imageViewPhoto = (ImageView) findViewById(R.id.imageViewLogo);
		customHeaders = (CustomHeaders) getIntent().getExtras().get("custom");
		idEnterprise = getIntent().getExtras().getInt("enterpriseId");
		Log.e("teste", "onCreate: " + idEnterprise);
		getInfos(customHeaders, idEnterprise);

	}

	private void getInfos(CustomHeaders customHeaders, Integer idEnterprise) {
		showProgress();
		Retrofit call = new Retrofit.Builder()
				.baseUrl(ApiService.BASE_URL)
				.addConverterFactory(GsonConverterFactory.create())
				.build();
		ApiService service = call.create(ApiService.class);

		Call<EnterpriseResp> getEnterprise =
				service.getEnterprise(idEnterprise,
						customHeaders.getAccessToken(),
						customHeaders.getClient(),
						customHeaders.getUid());


		getEnterprise.enqueue(new Callback<EnterpriseResp>() {
			@Override
			public void onResponse(Call<EnterpriseResp> call, Response<EnterpriseResp> response) {
				if (response.isSuccessful()) {
					getSupportActionBar().setTitle(response.body().getEnterprise().getEnterpriseName());
					textViewDesc.setText(response.body().getEnterprise().getDescription());
				} else {
					error(getString(R.string.try_again), getString(R.string.error));
				}
				mProgressDialog.hide();
			}

			@Override
			public void onFailure(Call<EnterpriseResp> call, Throwable t) {
				mProgressDialog.hide();
			}
		});
	}

	private void error(String message, String title) {
		new AlertDialog.Builder(this)
				.setIcon(R.drawable.ic_warning_amber_800_24dp)
				.setTitle(title)
				.setMessage(message)
				.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						getInfos(customHeaders, idEnterprise);
					}
				})
				.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						finish();
					}
				})
				.show();
	}

	private void showProgress() {
		this.runOnUiThread(new Runnable() {
			public void run() {
				mProgressDialog = new ProgressDialog(DescActivity.this);
				mProgressDialog.setIndeterminate(true);
				mProgressDialog.setMessage(getResources().getString(R.string.loading));
				mProgressDialog.setCancelable(false);
				mProgressDialog.show();
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				finish();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}


}
