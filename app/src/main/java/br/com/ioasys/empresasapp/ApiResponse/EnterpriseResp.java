package br.com.ioasys.empresasapp.ApiResponse;

import br.com.ioasys.empresasapp.Model.Enterprise;

/**
 * Created by William on 04/08/17.
 * Classe java para a
 */

public class EnterpriseResp {
	private Enterprise enterprise;

	public Enterprise getEnterprise() {
		return enterprise;
	}

	public void setEnterprise(Enterprise enterprise) {
		this.enterprise = enterprise;
	}
}
