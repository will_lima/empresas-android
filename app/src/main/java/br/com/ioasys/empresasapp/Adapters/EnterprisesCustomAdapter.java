package br.com.ioasys.empresasapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.ioasys.empresasapp.DescActivity;
import br.com.ioasys.empresasapp.Model.CustomHeaders;
import br.com.ioasys.empresasapp.Model.Enterprise;
import br.com.ioasys.empresasapp.R;

/**
 * Created by William on 02/08/17.
 * Adaptador para uma listview
 */

public class EnterprisesCustomAdapter extends ArrayAdapter<Enterprise> {
	private Context context;
	private CustomHeaders custom;

	public EnterprisesCustomAdapter(Context context, List<Enterprise> lista,CustomHeaders custom) {
		super(context, R.layout.enterprises_adapter, lista);
		this.context = context;
		this.custom = custom;
	}

	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
		LayoutInflater inflater = LayoutInflater.from(getContext());
		View view = inflater.inflate(R.layout.enterprises_adapter, parent, false);
		TextView name = (TextView)view.findViewById(R.id.textViewName);
		TextView type = (TextView)view.findViewById(R.id.textViewType);
		TextView country = (TextView)view.findViewById(R.id.texViewCountry);
		ImageView imageView = (ImageView) view.findViewById(R.id.imageViewEnterprisePhoto);
		final Enterprise enterprise = getItem(position);
		name.setText(enterprise.getEnterpriseName());
		type.setText(enterprise.getEnterpriseType().getEnterpriseTypeName());
		country.setText(enterprise.getCountry());
		view.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(context, DescActivity.class);
				intent.putExtra("enterpriseId", enterprise.getId());
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				intent.putExtra("custom",custom);
				context.startActivity(intent);
			}
		});
		return view;
	}

}
