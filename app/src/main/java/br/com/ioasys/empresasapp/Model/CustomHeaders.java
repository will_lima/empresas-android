package br.com.ioasys.empresasapp.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by William on 27/07/17.
 * Classe java para a
 */

public class CustomHeaders implements Parcelable {
	public static final Parcelable.Creator<CustomHeaders> CREATOR = new Parcelable.Creator<CustomHeaders>() {
		@Override
		public CustomHeaders createFromParcel(Parcel source) {
			return new CustomHeaders(source);
		}

		@Override
		public CustomHeaders[] newArray(int size) {
			return new CustomHeaders[size];
		}
	};
	private String accessToken;
	private String client;
	private String uid;

	public CustomHeaders(String accessToken, String client, String uid) {
		this.accessToken = accessToken;
		this.client = client;
		this.uid = uid;
	}

	protected CustomHeaders(Parcel in) {
		this.accessToken = in.readString();
		this.client = in.readString();
		this.uid = in.readString();
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.accessToken);
		dest.writeString(this.client);
		dest.writeString(this.uid);
	}

	@Override
	public String toString() {
		return "{\"CustomHeaders\":{"
				+ "\"accessToken\":\"" + accessToken + "\""
				+ ", \"client\":\"" + client + "\""
				+ ", \"uid\":\"" + uid + "\""
				+ "}}";
	}
}
