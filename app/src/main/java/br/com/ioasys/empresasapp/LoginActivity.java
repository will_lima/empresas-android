package br.com.ioasys.empresasapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import br.com.ioasys.empresasapp.ApiResponse.LoginResp;
import br.com.ioasys.empresasapp.Model.CustomHeaders;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {


	private Button buttonLogin;
	private TextInputLayout emailWrapper;
	private TextInputLayout passwordWrapper;
	private ProgressDialog mProgressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		getSupportActionBar().hide();
		buttonLogin =(Button) findViewById(R.id.buttonLogin);
		emailWrapper = (TextInputLayout) findViewById(R.id.textInputLayoutEmail);
		passwordWrapper = (TextInputLayout) findViewById(R.id.textInputLayoutPassword);

		buttonLogin.setOnClickListener(this);
	}

	/* Função que envia as informações para o servidor para tentar fazer o login
	 */
	private void tryLogin(String email, String senha) {
		Retrofit call = new Retrofit.Builder()
				.baseUrl(ApiService.BASE_URL)
				.addConverterFactory(GsonConverterFactory.create())
				.build();
		ApiService service = call.create(ApiService.class);
		Call<LoginResp> loginRequest = service.login(email,senha);
		loginRequest.enqueue(new Callback<LoginResp>() {
			@Override
			public void onResponse(Call<LoginResp> call, Response<LoginResp> response) {
				if(response.isSuccessful()){
					//TODO: Efetuar o login e ir para a tela de busca
					Log.e("kkk", "onResponse: "+response.headers());
					String acessToken= response.headers().get("access-token");
					String client = response.headers().get("client");
					String uid = response.headers().get("uid");
					CustomHeaders custom = new CustomHeaders(acessToken,client,uid);
					startActivitySearch(custom);
				}else{
					emailWrapper.setError(getResources().getString(R.string.credenciaisInvalidas));
					passwordWrapper.getEditText().setText("");
				}
				mProgressDialog.hide();

			}

			@Override
			public void onFailure(Call<LoginResp> call, Throwable t) {

				//t.printStackTrace();
				mProgressDialog.hide();
				error(getString(R.string.try_again), getString(R.string.errorConnect));
			}
		});


	}

	private void startActivitySearch(CustomHeaders custom) {
		Intent intent = new Intent(this, SearchActivity.class);
		intent.putExtra("headers",custom);
		startActivity(intent);
	}
	private void showDialog() {
		this.runOnUiThread(new Runnable() {
			public void run() {
				mProgressDialog = new ProgressDialog(LoginActivity.this);
				mProgressDialog.setIndeterminate(true);
				mProgressDialog.setMessage(getResources().getString(R.string.loading));
				mProgressDialog.setCancelable(false);
				mProgressDialog.show();
			}
		});
	}

	@Override
	public void onClick(View view) {
		showDialog();
		String email = emailWrapper.getEditText().getText().toString().trim();
		String senha = passwordWrapper.getEditText().getText().toString().trim();
		boolean erro = true;
		if(email.isEmpty()){
			emailWrapper.setError(getResources().getString(R.string.erroEmail));
			erro = false;
		}
		if(senha.isEmpty()){
			passwordWrapper.setError(getResources().getString(R.string.erroSenha));
			erro = false;
		}

		if (erro) {
			tryLogin(email,senha);
		}else{
			mProgressDialog.hide();
		}

	}

	private void error(String message, String title) {
		new AlertDialog.Builder(this)
				.setIcon(R.drawable.ic_warning_amber_800_24dp)
				.setTitle(title)
				.setMessage(message)
				.setNeutralButton("OK", null)
				.show();
	}
}
