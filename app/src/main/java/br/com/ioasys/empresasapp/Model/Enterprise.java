package br.com.ioasys.empresasapp.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by William on 02/08/17.
 * Classe java para a
 */

public class Enterprise {
	private int id;
	@SerializedName("email_enterprise")
	private String emailEnterprise;
	private String facebook;
	private String twitter;
	private String linkedin;
	private String phone;
	@SerializedName("own_enterprise")
	private String ownEnterprise;
	@SerializedName("enterprise_name")
	private String enterpriseName;
	private String photo;
	private String description;
	private String city;
	private String country;
	private Float value;
	@SerializedName("share_price")
	private Float sharePrice;
	@SerializedName("enterprise_type")
	private EnterpriseType enterpriseType;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmailEnterprise() {
		return emailEnterprise;
	}

	public void setEmailEnterprise(String emailEnterprise) {
		this.emailEnterprise = emailEnterprise;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	public String getTwitter() {
		return twitter;
	}

	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}

	public String getLinkedin() {
		return linkedin;
	}

	public void setLinkedin(String linkedin) {
		this.linkedin = linkedin;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getOwnEnterprise() {
		return ownEnterprise;
	}

	public void setOwnEnterprise(String ownEnterprise) {
		this.ownEnterprise = ownEnterprise;
	}

	public String getEnterpriseName() {
		return enterpriseName;
	}

	public void setEnterpriseName(String enterpriseName) {
		this.enterpriseName = enterpriseName;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Float getValue() {
		return value;
	}

	public void setValue(Float value) {
		this.value = value;
	}

	public Float getSharePrice() {
		return sharePrice;
	}

	public void setSharePrice(Float sharePrice) {
		this.sharePrice = sharePrice;
	}

	public EnterpriseType getEnterpriseType() {
		return enterpriseType;
	}

	public void setEnterpriseType(EnterpriseType enterpriseType) {
		this.enterpriseType = enterpriseType;
	}


	@Override
	public String toString() {
		return "{\"Enterprise\":{"
				+ "\"id\":\"" + id + "\""
				+ ", \"emailEnterprise\":\"" + emailEnterprise + "\""
				+ ", \"facebook\":\"" + facebook + "\""
				+ ", \"twitter\":\"" + twitter + "\""
				+ ", \"linkedin\":\"" + linkedin + "\""
				+ ", \"phone\":\"" + phone + "\""
				+ ", \"ownEnterprise\":\"" + ownEnterprise + "\""
				+ ", \"enterpriseName\":\"" + enterpriseName + "\""
				+ ", \"photo\":\"" + photo + "\""
				+ ", \"description\":\"" + description + "\""
				+ ", \"city\":\"" + city + "\""
				+ ", \"country\":\"" + country + "\""
				+ ", \"value\":\"" + value + "\""
				+ ", \"sharePrice\":\"" + sharePrice + "\""
				+ ", \"enterpriseType\":" + enterpriseType
				+ "}}";
	}
}
